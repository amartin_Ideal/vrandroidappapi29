﻿angular.module('app')
 .controller('printPaymentHistoryCtrl', ['$scope', '$rootScope', 'idealsApi', function ($scope, $rootScope, idealsApi) {

     try {
      
         $rootScope.message = "";
         $scope.userFLName = sessionStorage.userFLName;
         // get pay_history api method response and bind data  on html table
         var pay_hist = JSON.parse(sessionStorage['pay_hist'] || '{}');
         var payments = pay_hist.data.v1.payments;
         $scope.payHist = payments;

         // print page view
         $scope.printDirective = function (BodySection) {
             var innerContents = document.getElementById(BodySection).innerHTML;
             var popupWinindow = window.open();
             popupWinindow.document.open();
             popupWinindow.document.write('<html><head><style>table.right_align th {text-align: left;}</style></head><body onload="window.print()">' + innerContents + '</body></html>');
             popupWinindow.document.close();
            
         }

         // call payment_history api method 
         $scope.PaymentHistory = function () {
             idealsApi.payment_historyReq();
         }

         // logout click
         $scope.logoutClick = function () {
             idealsApi.logoutReq();
         };
     }
     catch(e)
     {
         console.log(e);
     }

}]);
   
 

