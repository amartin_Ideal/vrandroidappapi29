﻿

"use strict";

angular.module('app', ['ui.router', 'ngCookies', 'kendo.directives'])
  .config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/start');
        $stateProvider
          .state('start', {
              url: '/start',
              templateUrl: '../views/start.html',
              controller: 'startCtrl',
          })
          .state('start.search', {
              url: '/start.search',
              templateUrl: '../views/search.html',
              controller: 'searchCtrl',
          })
               .state('start.search.item', {
                   url: '/start.search.item',
                   templateUrl: '../views/item.html',
                   controller: 'itemCtrl',
               })
          .state('start.searchNotFound', {
              url: '/start.searchNotFound',
              templateUrl: '../views/searchNotFound.html',
              controller: 'searchNotFoundCtrl',
          }).
         state('cartEmpty', {
             url: '/cartEmpty',
             templateUrl: '../views/cartEmpty.html',
             controller: 'cartEmptyCtrl',
         }).
         state('cartNotEmpty', {
             url: '/cartNotEmpty',
             templateUrl: '../views/cartNotEmpty.html',
             controller: 'cartNotEmptyCtrl',
         }).

        state('payDeposit', {
            url: '/payDeposit',
            templateUrl: '../views/pay_deposit.html',
            controller: 'payDepositCtrl',
        }).
        state('payDecline', {
            url: '/payDecline',
            templateUrl: '../views/payment_decline.html',
            controller: 'payDeclineCtrl',
        }).
        state('paySuccess', {
            url: '/paySuccess',
            templateUrl: '../views/payment_success.html',
            controller: 'paySuccessCtrl',
        });
    }
  ]);