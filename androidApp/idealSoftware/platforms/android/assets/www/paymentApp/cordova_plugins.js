cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-tts.tts",
        "file": "plugins/cordova-plugin-tts/www/tts.js",
        "pluginId": "cordova-plugin-tts",
        "clobbers": [
            "TTS"
        ]
    },
    {
        "id": "at.oneminutedistraction.phonenumber.PhoneNumber",
        "file": "plugins/at.oneminutedistraction.phonenumber/www/phonenumber.js",
        "pluginId": "at.oneminutedistraction.phonenumber",
        "clobbers": [
            "phonenumber"
        ]
    },
    {
        "id": "org.loicknuchel.cordova.deviceaccounts.DeviceAccounts",
        "file": "plugins/org.loicknuchel.cordova.deviceaccounts/www/DeviceAccounts.js",
        "pluginId": "org.loicknuchel.cordova.deviceaccounts",
        "clobbers": [
            "plugins.DeviceAccounts"
        ]
    },
    {
        "id": "cordova-plugin-android-permissions.Permissions",
        "file": "plugins/cordova-plugin-android-permissions/www/permissions.js",
        "pluginId": "cordova-plugin-android-permissions",
        "clobbers": [
            "cordova.plugins.permissions"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.2",
    "cordova-plugin-tts": "0.2.3",
    "at.oneminutedistraction.phonenumber": "0.0.1",
    "org.loicknuchel.cordova.deviceaccounts": "0.0.1",
    "cordova-plugin-android-permissions": "1.0.0"
};
// BOTTOM OF METADATA
});