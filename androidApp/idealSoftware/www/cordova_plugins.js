cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-tts.tts",
        "file": "plugins/cordova-plugin-tts/www/tts.js",
        "pluginId": "cordova-plugin-tts",
        "clobbers": [
            "TTS"
        ]
    },
    {
        "id": "cordova-plugin-network-information.network",
        "file": "plugins/cordova-plugin-network-information/www/network.js",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "id": "cordova-plugin-network-information.Connection",
        "file": "plugins/cordova-plugin-network-information/www/Connection.js",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "Connection"
        ]
    },
    {
        "id": "com.red_folder.phonegap.plugin.backgroundservice.BackgroundService",
        "file": "plugins/com.red_folder.phonegap.plugin.backgroundservice/www/backgroundService.js",
        "pluginId": "com.red_folder.phonegap.plugin.backgroundservice"
    },
    {
        "id": "cordova-sqlite-storage.SQLitePlugin",
        "file": "plugins/cordova-sqlite-storage/www/SQLitePlugin.js",
        "pluginId": "cordova-sqlite-storage",
        "clobbers": [
            "SQLitePlugin"
        ]
    },
    {
        "id": "at.oneminutedistraction.phonenumber.PhoneNumber",
        "file": "plugins/at.oneminutedistraction.phonenumber/www/phonenumber.js",
        "pluginId": "at.oneminutedistraction.phonenumber",
        "clobbers": [
            "phonenumber"
        ]
    },
    {
        "id": "org.loicknuchel.cordova.deviceaccounts.DeviceAccounts",
        "file": "plugins/org.loicknuchel.cordova.deviceaccounts/www/DeviceAccounts.js",
        "pluginId": "org.loicknuchel.cordova.deviceaccounts",
        "clobbers": [
            "plugins.DeviceAccounts"
        ]
    },
    {
        "id": "cordova-plugin-device.device",
        "file": "plugins/cordova-plugin-device/www/device.js",
        "pluginId": "cordova-plugin-device",
        "clobbers": [
            "device"
        ]
    },
    {
        "id": "cordova-plugin-android-permissions.Permissions",
        "file": "plugins/cordova-plugin-android-permissions/www/permissions.js",
        "pluginId": "cordova-plugin-android-permissions",
        "clobbers": [
            "cordova.plugins.permissions"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.2",
    "cordova-plugin-tts": "0.2.3",
    "cordova-plugin-network-information": "1.3.3",
    "com.red_folder.phonegap.plugin.backgroundservice": "2.0.0",
    "cordova-sqlite-storage": "2.0.4",
    "at.oneminutedistraction.phonenumber": "0.0.1",
    "org.loicknuchel.cordova.deviceaccounts": "0.0.1",
    "cordova-plugin-device": "2.0.2",
    "cordova-plugin-android-permissions": "1.0.0"
};
// BOTTOM OF METADATA
});